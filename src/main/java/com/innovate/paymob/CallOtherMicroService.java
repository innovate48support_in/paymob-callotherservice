package com.innovate.paymob;

import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.innovate.paymob.businessobjects.ItemsResponse;
import com.innovate.paymob.businessobjects.OfferResponse;
import com.innovate.paymob.businessobjects.TransferResponse;

public class CallOtherMicroService {

	public static final String SEND_MONEY_MICRO_SERVICE_URL 
	= "http://localhost:8080/SendMoneyApplication/rest/sendMoney?debitMobileNumber=9898989898&creditMobileNumber=7979797979&amount=500";
	
	public static final String OFFERS_MICRO_SERVICE_URL 
	="http://localhost:8099/PaymentHistoryApp/rest/userOffer?mobile=9898989898";
	
	public static final String BUCKET_LIST_MICRO_SERVICE_URL
	="http://localhost:8088/BucketList/rest/bucketListItems?itemsInfo=items";
	
	//Sample Case Method to call SEND_MONEY MICRO_SERVICE
	public TransferResponse callSendMoneyMicroService(Long debitMobileNumber,
														Long creditMobileNumber
														, double amount) {
		UriComponentsBuilder builder = UriComponentsBuilder
			    						.fromUriString(SEND_MONEY_MICRO_SERVICE_URL)
									    // Add query parameter
									    .queryParam("debitMobileNumber", debitMobileNumber)
									    .queryParam("creditMobileNumber", creditMobileNumber)
									    .queryParam("amount", amount);

		RestTemplate restTemplate = new RestTemplate();
		TransferResponse response = restTemplate.postForObject(
												  builder.toUriString(), null, TransferResponse.class);
		return response;
	}

	//Sample Case Method to call OFFERS MICRO_SERVICE
	public OfferResponse callOffersMicroService(Long mobile) {
		OfferResponse response =null;
		UriComponentsBuilder builder = UriComponentsBuilder
			    						.fromUriString(OFFERS_MICRO_SERVICE_URL)
									    // Add query parameter
									    .queryParam("mobile", mobile);

		RestTemplate restTemplate = new RestTemplate();
		response = restTemplate.postForObject(
								 builder.toUriString(), null, OfferResponse.class);
		return response;
	}
		
	//Sample Case Method to call BucketList MICRO_SERVICE
	public ItemsResponse callBucketListMicroService(String itemsInfo) {
		ItemsResponse response =null;
		UriComponentsBuilder builder = UriComponentsBuilder
			    						.fromUriString(BUCKET_LIST_MICRO_SERVICE_URL)
									    // Add query parameter
									    .queryParam("itemsInfo", itemsInfo);

		RestTemplate restTemplate = new RestTemplate();
		response = restTemplate.postForObject(
								 builder.toUriString(), null, ItemsResponse.class);
		return response;
	}

	
}
