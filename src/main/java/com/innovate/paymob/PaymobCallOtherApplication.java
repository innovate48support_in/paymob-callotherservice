package com.innovate.paymob;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaymobCallOtherApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaymobCallOtherApplication.class, args);
	}

}
